import examples from './examples';

export default {
  examples,
  bootstrapComponent: 'b-breadcrumb',
};
