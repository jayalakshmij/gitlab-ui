import * as description from './button_group.md';
import examples from './examples';

export default {
  description,
  examples,
  bootstrapComponent: 'b-button-group',
};
