import * as description from './line.md';
import examples from './examples';

export default {
  description,
  examples,
};
