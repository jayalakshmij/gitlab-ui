import LegendBasicExample from './legend.basic.example.vue';

export default [
  {
    name: 'Basic',
    items: [
      {
        id: 'chart-legend-basic',
        name: 'Basic',
        description: 'Basic Chart Legend',
        component: LegendBasicExample,
      },
    ],
  },
];
